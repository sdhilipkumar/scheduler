package docker

import (
	"context"
	"strings"
	"testing"

	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller"

	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
)

func callTestFunc(t *testing.T, f func() error, msg string) {
	err := f()
	if err != nil {
		t.Fatalf("Error msg=%v err=%v", msg, err)
	}
}

func TestCreateContainer(t *testing.T) {

	c1 := NewDocker("node-1")
	//Start a workload that prints completed and sleeps for 3 seconds
	work := controller.Work{
		Name:    "work1",
		Image:   "bash:3.0",
		Command: []string{"/usr/local/bin/bash", "-c", "echo Completed; sleep 3"},
	}
	err := c1.Start(&work)
	if err != nil {
		t.Fatalf("Error occured err=%v", err)
	}

	//Remove the container when the test finishes.
	defer func() {
		err := c1.Client.ContainerRemove(context.Background(), c1.iD, types.ContainerRemoveOptions{})
		if err != nil {
			t.Fatalf("Unable to remove the contaienr err = %", err)
		}
	}()

	//Container should be running
	if c1.Status != controller.WorkStatusRunning {
		t.Fatalf("Work status expected =%v Obtainted=%v", controller.WorkStatusRunning, c1.Status)
	}

	//Wait for the container to finish
	callTestFunc(t, c1.Wait, "Waiting for container")

	//The status of the work should be completed
	if c1.Status != controller.WorkStatusCompleted {
		t.Fatalf("Work status expected =%v Obtainted=%v", controller.WorkStatusCompleted, c1.Status)
	}

	//Output should be completed
	output := c1.Logs()
	if !strings.Contains(output, "Completed") {
		t.Fatalf("Work output expected=Completed obtained=%v", output)
	}
}

func TestStop(t *testing.T) {

	c1 := NewDocker("node-1")
	//Start a workload that prints completed and sleeps for 3 seconds
	work := controller.Work{
		Name:    "work1",
		Image:   "bash:3.0",
		Command: []string{"/usr/local/bin/bash", "-c", "echo Completed; sleep 3"},
	}
	err := c1.Start(&work)
	if err != nil {
		t.Fatalf("Error occured while staring a container err=%v", err)
	}

	//Now Stop the container
	callTestFunc(t, c1.Stop, "Stopping Container")

	fil := filters.NewArgs()
	fil.Add("name", c1.nodename)

	//Check if the container is running
	containers, err := c1.Client.ContainerList(context.Background(), types.ContainerListOptions{All: true, Filters: fil})
	if err != nil {
		t.Fatalf("Unable to list containers err=%v", err)
	}

	found := false
	for _, c := range containers {
		if c.ID == c1.iD {
			found = true
		}
	}
	if found {
		t.Fatalf("Contianer id=%v should not be running", c1.iD)
	}
}

func TestList(t *testing.T) {

	//Create numContainers containers and list them.
	numContainers := 3
	for i := 1; i <= numContainers; i++ {
		c := NewDocker("node-1")
		w := &controller.Work{
			Name:    fmt.Sprintf("work%d", i),
			Image:   "bash:3.0",
			Command: []string{"/usr/local/bin/bash", "-c", "echo Completed; sleep 3"},
		}
		callTestFunc(t, func() error {
			return c.Start(w)
		}, "Start Container")
		go callTestFunc(t, c.Wait, "Waiting for container")
		defer callTestFunc(t, c.Stop, "Stopping Container")
	}

	//Now list the available containers we should only get numContainers
	de := Engine{NodeName: "node-1"}
	containers := de.List()
	if len(containers) != 3 {
		t.Fatalf("Expeced = 3 Obtained=%d", len(containers))
	}

}
