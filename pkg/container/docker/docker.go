package docker

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"

	containerinterface "bitbucket.org/sdhilipkumar/scheduler/pkg/container"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	log "github.com/golang/glog"
)

//A Single client is enough for all the container instances.
var globalClient *client.Client

//Engine implementation of container engine.
type Engine struct {
	NodeName string
}

//New creates a new container
func (de *Engine) New() containerinterface.Container {
	return NewDocker(de.NodeName)
}

func (de *Engine) List() []containerinterface.Container {

	var result []containerinterface.Container

	ctx := context.Background()

	fil := filters.NewArgs()
	fil.Add("name", de.NodeName)

	//List of containers running started by this node
	containers, err := globalClient.ContainerList(ctx, types.ContainerListOptions{All: true, Filters: fil})
	if err != nil {
		log.Errorf("Unable to list containers err=%v", err)
		return result
	}

	for _, c := range containers {
		d := NewDocker(de.NodeName)
		d.iD = c.ID
		d.ContainerName = c.Labels["name"]
		switch c.State {
		case "running":
			d.Status = controller.WorkStatusRunning
		case "exited":
			//We wont know at this point if the container has failed or completed.
			d.Status = controller.WorkStatusCompleted
		}
		result = append(result, d)
	}
	return result
}

//Docker Implements Container interface
type docker struct {
	iD            string
	nodename      string
	ContainerName string
	Client        *client.Client
	controller.Work
}

//NewDocker creates a new docker instance which can be started / stopped etc.,
func NewDocker(name string) *docker {
	return &docker{
		nodename: name,
		Client:   globalClient,
	}
}

//Start starts a container (including image pull, isolation etc)
func (d *docker) Start(work *controller.Work) error {

	ctx := context.Background()

	// container name should be pre-fixed with the nodename
	d.ContainerName = fmt.Sprintf("%s_%s", d.nodename, work.Name)

	//Update the resource information/requirement from 'work' to 'd'.
	d.updateWorkInfo(work)

	//Mark the status as Failed by default
	d.Status = controller.WorkStatusFailed

	// Pull Image if required
	if !d.checkImageExists(work.Image) {
		imgResp, err := d.Client.ImagePull(ctx, work.Image, types.ImagePullOptions{})
		if err != nil {
			log.Errorf("Unable to pull image err=%v", err)
			return err
		}

		if err := d.checkPullImage(imgResp); err != nil {
			log.Error(err)
			return err
		}
	}

	// Create Container
	containerConfig := container.Config{Image: work.Image, Cmd: work.Command, Labels: map[string]string{"name": d.ContainerName}}
	resource := container.Resources{CPUCount: int64(work.Requirement.CPU), DiskQuota: int64(work.Requirement.DISK), Memory: int64(work.Requirement.MEM)}
	hostConfig := container.HostConfig{Resources: resource}
	resp, err := d.Client.ContainerCreate(ctx, &containerConfig, &hostConfig, nil, d.ContainerName)
	if err != nil {
		log.Errorf("Unable to create container err=%v", err)
		return err
	}
	d.iD = resp.ID

	// Start Container
	if err := d.Client.ContainerStart(ctx, d.iD, types.ContainerStartOptions{}); err != nil {
		log.Errorf("Unable to start container err=%v", err)
		return err
	}

	//Now mark the container as running.
	d.Status = controller.WorkStatusRunning

	return nil
}

//Waits for the container to be terminated.
func (d *docker) Wait() error {

	var err error

	ctx := context.Background()
	statusCh, errCh := d.Client.ContainerWait(ctx, d.iD, container.WaitConditionNotRunning)
	select {
	case err = <-errCh:
		log.Errorf("Unable to wait for the container = %v", err)
		d.Status = controller.WorkStatusFailed

	case status := <-statusCh:
		if status.Error != nil && status.StatusCode != 0 {
			d.Status = controller.WorkStatusFailed
			return fmt.Errorf("Container filed error %v StatusCode=%d", status.Error, status.StatusCode)
		} else {
			d.Status = controller.WorkStatusCompleted
		}
	}

	return err
}

//Stop stops container (kills) a running container, and removes it if it had completed
func (d *docker) Stop() error {

	ctx := context.Background()
	if d.Status == controller.WorkStatusRunning {
		err := d.Client.ContainerKill(ctx, d.iD, "SIGKILL")
		if err != nil {
			log.Errorf("Unable to kill the container err=%v", err)
			return err
		}
	}

	err := d.Client.ContainerRemove(ctx, d.iD, types.ContainerRemoveOptions{})
	if err != nil {
		log.Errorf("Unable to remove the container from docker host err=%v", err)
		return err
	}

	return nil
}

func (d *docker) updateWorkInfo(work *controller.Work) {
	d.Image = work.Image
	d.Command = work.Command
	d.Requirement.CPU = work.Requirement.CPU
	d.Requirement.MEM = work.Requirement.MEM
	d.Requirement.DISK = work.Requirement.DISK
}

//Logs retrieves log of the container
func (d *docker) Logs() string {

	output := bytes.NewBufferString("")
	ctx := context.Background()

	// Configure output
	logReader, err := d.Client.ContainerLogs(ctx, d.iD, types.ContainerLogsOptions{ShowStdout: true, ShowStderr: true})
	if err != nil {
		log.Errorf("Unable to fetch logs for the container err=%v", err)
		return ""
	}

	io.Copy(output, logReader)
	return output.String()
}

//RunStatus returns the current status of container runtime.
func (d *docker) RunStatus() string {
	return d.Status
}

//TODO: If docker is not the de-facto container runtime then we cannot use log.Fatalf()
//TODO: Device a better plan to handle this error
func init() {
	var err error
	//Create a default client to be used by all the instances
	globalClient, err = client.NewEnvClient()
	if err != nil {
		log.Fatalf("Unable to initialize docker")
	}
}

func (d *docker) checkPullImage(resp io.ReadCloser) error {
	outPut := bytes.NewBufferString("")
	defer resp.Close()

	_, err := io.Copy(outPut, resp)
	if err != nil {
		return err
	}

	outPutStr := outPut.String()

	if strings.Contains(outPutStr, "Image is up to date") || strings.Contains(outPutStr, "Downloaded newer image for") {
		return nil
	}

	return fmt.Errorf("Image download failed")
}

func (d *docker) checkImageExists(image string) bool {

	images, err := d.Client.ImageList(context.Background(), types.ImageListOptions{})
	if err != nil {
		log.Errorf("Unable to list images err = %v", err)
		return false
	}
	for _, img := range images {
		for _, tag := range img.RepoTags {
			if strings.Contains(image, tag) {
				return true
			}
		}
	}
	return false
}
