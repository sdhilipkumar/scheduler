package container

import (
	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller"
)

//ContainerEngine a common abstraction for creating connection to container backend
type ContainerEngine interface {
	//Creates a new conenction to the container interface
	New() Container
	List() []Container
}

//Container a common abstraction for all the container implementation
type Container interface {

	// Start a container this may include
	// pulling image [if applicable]
	// Creating isolation
	// starting the container etcd.,
	Start(work *controller.Work) error

	// Stop the container, this should be ideally blocking until the container is killed
	Stop() error

	//Retrieves logs of the container as a string
	Logs() string

	//Status returns status of the container
	RunStatus() string

	//Wait waits for the container to terminate
	Wait() error
}
