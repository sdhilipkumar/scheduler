package etcd

import (
	"testing"

	"github.com/coreos/etcd/integration"
)

//SetupTestEtcd setup a test etcd cluster
//Some test-utils if exposed can be re-sued from other modules
//Create a new etcd instance and its client.
func SetupTestEtcd(t *testing.T) (*integration.ClusterV3, *Etcd) {

	var etcd Etcd

	cluster := integration.NewClusterV3(t, &integration.ClusterConfig{Size: 1})
	client, err := integration.NewClientV3(cluster.Members[0])
	if err != nil {
		t.Fatalf("Unable to fetch etcd clients")
	}

	etcd.Client = client

	return cluster, &etcd
}
