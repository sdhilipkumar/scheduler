package etcd

import (
	"context"
	"time"

	"bitbucket.org/sdhilipkumar/scheduler/pkg/storage"

	v3 "github.com/coreos/etcd/clientv3"
	lock "github.com/coreos/etcd/clientv3/concurrency"
	"k8s.io/client-go/util/workqueue"
)

//Etcd this implements storage to enable etcd as our storage backend.
type Etcd struct {
	Client *v3.Client
}

//New creates a new etcd objects.
func New(endpoints []string, timeout time.Duration) (*Etcd, error) {

	var etcd Etcd

	client, err := v3.New(v3.Config{Endpoints: endpoints, DialTimeout: timeout})
	if err != nil {
		return nil, err
	}
	etcd.Client = client

	return &etcd, nil
}

//Initialize initializes the etcd store.
func (e *Etcd) Initialize() error {

	return nil
}

//Watch watches a key (and its prefix by default) returns a workqueue, this is async in nature.
func (e *Etcd) Watch(key string) *workqueue.Type {

	opts := []v3.OpOption{v3.WithPrefix(), v3.WithCreatedNotify(), v3.WithPrevKV()}

	watchCh := e.Client.Watch(context.Background(), key, opts...)
	workQueue := workqueue.NewNamed(key)

	goRoutineCreated := make(chan bool)

	go func() {
		close(goRoutineCreated)
		for resp := range watchCh {
			if resp.Canceled {
				workQueue.ShutDown()
				return
			}
			for _, ev := range resp.Events {
				element := storage.KVWatch{}

				//Get the type of the operation that had happened.
				switch {
				case ev.IsCreate():
					element.OpType = storage.KVCreated
				case ev.IsModify():
					element.OpType = storage.KVUpdated
				case ev.Type == v3.EventTypeDelete:
					element.OpType = storage.KVDeleted
				}

				element.Current.Key = ev.Kv.Key
				element.Current.Value = ev.Kv.Value
				element.Current.Version = ev.Kv.Version

				if ev.PrevKv != nil {
					element.Old.Key = ev.PrevKv.Key
					element.Old.Value = ev.PrevKv.Value
					element.Old.Version = ev.PrevKv.Version
				}
				workQueue.Add(&element)
			}
		}
	}()

	//Wait a bit for the go-routine to be actually created.
	<-goRoutineCreated

	return workQueue
}

//Put creates / update a key value, optionally can be locked for consistency.
func (e *Etcd) Put(key string, value string, lockIt bool) error {

	if lockIt {
		lck, err := e.lockKey(key)
		if err != nil {
			return err
		}
		defer lck.Unlock(context.TODO())
	}

	_, err := e.Client.Put(context.Background(), key, value)
	return err
}

//Get gets a value of a given key from storage
func (e *Etcd) Get(key string) ([]string, error) {

	var result []string

	resp, err := e.Client.Get(context.Background(), key)
	if err != nil {
		return nil, err
	}
	for _, value := range resp.Kvs {
		result = append(result, string(value.Value))
	}

	return result, nil
}

//Delete deletes key from storage, optionally can be locked for consistency
func (e *Etcd) Delete(key string, lockIt bool) error {

	if lockIt {
		lck, err := e.lockKey(key)
		if err != nil {
			return err
		}
		defer lck.Unlock(context.TODO())
	}

	_, err := e.Client.Delete(context.Background(), key)
	if err != nil {
		return err
	}

	return nil
}

//Format the storage bring it back to cleanslate.
func (e *Etcd) Format(key string) error {

	_, err := e.Client.Delete(context.Background(), key, v3.WithPrefix())
	if err != nil {
		return err
	}

	return nil
}

//List all the keys with 'key' as its prefix
func (e *Etcd) List(key string) ([]string, error) {

	var result []string

	resp, err := e.Client.Get(context.Background(), key, v3.WithPrefix(), v3.WithKeysOnly())
	if err != nil {
		return nil, err
	}
	for _, kv := range resp.Kvs {
		result = append(result, string(kv.Key))
	}

	return result, nil

}

func (e *Etcd) lockKey(key string) (*lock.Mutex, error) {
	s, err := lock.NewSession(e.Client, lock.WithTTL(60))
	if err != nil {
		return nil, err
	}

	//Add a prefix to the lock as this disrupts watch on keys
	lckKey := "lock/" + key
	lck := lock.NewMutex(s, lckKey)

	//Use TODO context as our default context may timeout
	err = lck.Lock(context.TODO())

	if err != nil {
		return nil, err
	}

	return lck, nil
}
