package etcd

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/sdhilipkumar/scheduler/pkg/storage"
)

func TestWatch(t *testing.T) {
	cluster, etcd := SetupTestEtcd(t)
	defer cluster.Terminate(t)

	key := "Hello"
	originalValue := "World"
	newValue := "World1"

	//Create a watch on the key Hello
	Q := etcd.Watch(key)

	//Create key
	err := etcd.Put(key, originalValue, false)
	if err != nil {
		t.Fatalf("Error adding a key to etcd %v err=%v", key, err)
	}

	//Update the key
	err = etcd.Put(key, newValue, false)
	if err != nil {
		t.Fatalf("Error updating a key to etcd %v err=%v", key, err)
	}

	//Delete the Key
	err = etcd.Delete(key, false)
	if err != nil {
		t.Fatalf("Error deleting a key to etcd %v err=%v", key, err)
	}

	//So far we should have received three items in the queue.
	t.Run("CREATE-KEY", func(t *testing.T) {

		item, shutdown := Q.Get()
		if shutdown {
			t.Fatalf("Queue should be valid but is now asked to shutdown")
		}
		defer Q.Done(item)

		watchItem, isValid := item.(*storage.KVWatch)
		if !isValid {
			t.Fatalf("Unable to typecaset to storage.KVWatch")
		}

		t.Logf("WatchItem = %+v", *watchItem)

		if string(watchItem.Current.Value) != originalValue {
			t.Fatalf("Expected value =%v, Obtained Value =%v", newValue, string(watchItem.Current.Value))
		}

		if watchItem.OpType != storage.KVCreated {
			t.Fatalf("Operation type should be Created but recived=%v", watchItem.OpType)
		}

	})

	t.Run("UPDATE-KEY", func(t *testing.T) {

		item, shutdown := Q.Get()
		if shutdown {
			t.Fatalf("Queue should be valid but is now asked to shutdown")
		}
		defer Q.Done(item)

		watchItem, isValid := item.(*storage.KVWatch)
		if !isValid {
			t.Fatalf("Unable to typecaset to storage.KVWatch")
		}

		t.Logf("WatchItem = %+v", *watchItem)

		if watchItem.OpType != storage.KVUpdated {
			t.Fatalf("Operation type should be Created but recived=%v", watchItem.OpType)
		}

		if string(watchItem.Current.Value) != newValue {
			t.Fatalf("Expected value =%v, Obtained Value =%v", newValue, string(watchItem.Current.Value))
		}

		if string(watchItem.Old.Value) != originalValue {
			t.Fatalf("Expected value =%v, Obtained Value =%v", newValue, string(watchItem.Current.Value))
		}

		if watchItem.Current.Version != 2 {
			t.Fatalf("Expected Version = 2 Obtained version=%v", watchItem.Current.Version)
		}

		if watchItem.OpType != storage.KVUpdated {
			t.Fatalf("Operation type should be Created but recived=%v", watchItem.OpType)
		}

	})

	t.Run("DELETE-KEY", func(t *testing.T) {

		item, shutdown := Q.Get()
		if shutdown {
			t.Fatalf("Queue should be valid but is now asked to shutdown")
		}
		defer Q.Done(item)

		watchItem, isValid := item.(*storage.KVWatch)
		if !isValid {
			t.Fatalf("Unable to typecaset to storage.KVWatch")
		}

		t.Logf("WatchItem = %+v", *watchItem)

		if watchItem.OpType != storage.KVDeleted {
			t.Fatalf("Operation type should be Created but recived=%v", watchItem.OpType)
		}

	})
}

func TestGetPut(t *testing.T) {

	cluster, etcd := SetupTestEtcd(t)
	defer cluster.Terminate(t)

	key := "Hello"
	originalValue := "World"

	t.Run("Simple-Get-Put", func(t *testing.T) {

		err := etcd.Put(key, originalValue, false)
		if err != nil {
			t.Fatalf("Error adding a key to etcd %v err=%v", key, err)
		}

		result, err := etcd.Get(key)
		if err != nil {
			t.Fatalf("Unable to Get(%s) value err=%v", key, err)
		}

		if len(result) != 1 {
			t.Fatalf("Expected =%v Obtained=%v", 1, len(result))
		}

		if result[0] != originalValue {
			t.Fatalf("Expected =%v Obtained=%v", originalValue, result[0])
		}
	})

	t.Run("Lock-Put", func(t *testing.T) {

		lck, err := etcd.lockKey(key)
		if err != nil {
			t.Fatalf("Unable to lock key=%v err=%v", key, err)
		}

		doneCh := make(chan bool)

		go func() {
			//Put with lock = true, this should block
			err := etcd.Put(key, originalValue, true)
			if err != nil {
				t.Fatalf("Error adding a key to etcd %v err=%v", key, err)
			}
			doneCh <- true
		}()

		//First time wait on the channel, we should not receive from doneCh
		select {
		case <-doneCh:
			t.Fatalf("It should be locked, we should not have recived frome this channel")
		case <-time.After(time.Second):
		}

		lck.Unlock(context.TODO())

		//After unlock we should receive from doneCh
		select {
		case <-doneCh:
		case <-time.After(time.Second):
			t.Fatalf("It should be unlocked and we should have have recived from doneCh")
		}

	})

}
