package storage

import (
	"fmt"
	"k8s.io/client-go/util/workqueue"
)

// KV Key value structure
type KV struct {
	Version int64
	Key     []byte `json:string`
	Value   []byte `json:string`
}

//Watch Operation Type Constants
const (
	KVCreated int = iota //A new KV pair is created
	KVDeleted            //A KV Pair is deleted
	KVUpdated            //A KV Pair is updated (will have old and new KVs)
)

// KVWatch contains KV pair which is used by Watch represents the previous and current state of the KV.
type KVWatch struct {
	OpType  int //Operation type
	Old     KV  //Old KV in case it was modified
	Current KV  //Updated KV
}

//ToString returns a string representation for debugging cannot use json.Marshall directly.
func (kvwatch *KVWatch) ToString() string {

	var output string
	switch kvwatch.OpType {
	case KVCreated:
		output = "[Op=Created; "
		output += fmt.Sprintf("Key=%s value=%s; ", string(kvwatch.Current.Key), string(kvwatch.Current.Value))
	case KVUpdated:
		output = "[Op=Updated; "
		output += fmt.Sprintf("Key=%s value=%s; ", string(kvwatch.Current.Key), string(kvwatch.Current.Value))
		output += fmt.Sprintf("OldKey=%s Oldvalue=%s; ", string(kvwatch.Old.Key), string(kvwatch.Old.Value))
	case KVDeleted:
		output = "[Op=Deleted; "
		output += fmt.Sprintf("OldKey=%s Oldvalue=%s; ", string(kvwatch.Old.Key), string(kvwatch.Old.Value))
	}

	output += "]"

	return output

}

//Storage is the basic interface every KV store must implement.
type Storage interface {

	//Initialize the store
	Initialize() error

	//Watch on a particular Key, should return a workqueue with KVWatch type
	Watch(string) *workqueue.Type

	//Put creates / update a key value
	Put(key string, value string, lockIt bool) error

	//Get a value of a given key
	Get(string) ([]string, error)

	//Remove/Delete this key from storage
	Delete(key string, lockIt bool) error

	//Format the storage bring it back to cleanslate.
	Format(key string) error

	//List all the content of the key (recursively)
	List(key string) ([]string, error)
}
