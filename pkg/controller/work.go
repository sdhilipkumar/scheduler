package controller

import (
	"encoding/json"

	"bitbucket.org/sdhilipkumar/scheduler/pkg/storage"

	"fmt"
	log "github.com/golang/glog"
)

//Constants for Work status
const (
	WorkStatusPending   = "Pending"   //Work is yet to be scheduled
	WorkStatusRunning   = "Running"   //Work is running
	WorkStatusCompleted = "Completed" //Work has completed execution
	WorkStatusFailed    = "Failed"    //Work has Failed to run
)

//WorkStatus general representation of workstatus
type WorkStatus struct {
	Status string //Status of the container
	Output string //Output of the container if possible
}

//Work represents a unit of work to be run by a container
type Work struct {
	Name        string   //Name of the workload unique across the cluster
	Image       string   //Image of the workload (docker)
	Command     []string //Command to be executed
	Status      string   //Current satuts of the workload
	Requirement Resource //Resource requirement of the workload
	Scheudled   string   //The node it is scheduled at
}

//LoadWork Loads a work from store to memeory
func LoadWork(key string, store storage.Storage) (*Work, error) {

	var result Work

	values, err := store.Get(WorkloadPath + key)
	if err != nil {
		log.Errorf("Unable to fetch work from store err=%v", err)
		return nil, err
	}

	if len(values) != 1 {
		log.Errorf("Cannot have more than one work")
		return nil, fmt.Errorf("Cannot have more than one work")
	}

	err = json.Unmarshal([]byte(values[0]), &result)
	if err != nil {
		log.Errorf("Unmarshall error =%v", err)
		return nil, err
	}

	return &result, nil
}

//StoreWork stores work in store
func StoreWork(work *Work, store storage.Storage) error {

	value, err := json.Marshal(work)
	if err != nil {
		log.Errorf("Marshall error = %v", err)
		return err
	}

	err = store.Put(WorkloadPath+work.Name, string(value), true)
	if err != nil {
		log.Errorf("Unable to store work err=%v", err)
		return err
	}

	return nil
}
