package node

import (
	"fmt"
	"os"
	"path"
	"sync"
	"time"

	"bitbucket.org/sdhilipkumar/scheduler/pkg/container"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/storage"

	log "github.com/golang/glog"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
	"k8s.io/apimachinery/pkg/util/json"
	"k8s.io/client-go/util/workqueue"
)

//package implements node controller
//Input: watches 'SingleNodeWorks' path
//Output: Writes out status of the container at 'SingleNodeStatus'
//Action: Creates and Manages containers corresponding to the work.

const defaultNodeUpdateFrequency = time.Second * 5

type workStatusNode struct {
	Name string
	controller.WorkStatus
	UpdateNodeStatus bool
}

//ResourceCapacity to resource level status of a node, total capacity vs available capacity
type ResourceCapacity struct {
	//A lock to sync the access to this struct.
	sync.Mutex
	//Total hardware capacity of the node
	Total controller.Resource //Total capacity
	//Available hardware capacity of the node (considering the processes running)
	Available controller.Resource //Available Capacity
}

//Node a structure representing a node
type Node struct {
	//Name of the node
	Name string
	//Hostname of the node
	Hostname string
	//NodeStoreKey the key path used to store node information
	NodeStoreKey string `json:"-"`
	//NodeWorksKey the key path used to store node work to be started (input for node)
	NodeWorksKey string `json:"-"`
	//NodeStatusKey the key path used to store node where node writes the status (output of node)
	NodeStatusKey string `json:"-"`
	//Hardware resources at the node
	Resource *ResourceCapacity
	//Map of Work assigned to this Node INPUT (ignored while marshalling)
	Works map[string]*controller.Work `json:"-"`
	//Map of work status (ignored while marshalling)
	Status map[string]*controller.WorkStatus `json:"-"`
	//Heart beat timestamp
	Hearbeat time.Time
	//The storage layer to use for communication
	store storage.Storage
	//The queue
	q *workqueue.Type
	//Hooks for callbacks
	hooks *controller.Hooks
	//Container engine and map of running containers
	containerEngine container.ContainerEngine
	//Map of containers managed by this node
	containers map[string]container.Container
	//status channel
	workStatusCh chan *workStatusNode
}

//New creates a new node
func New(name string, store storage.Storage, engine container.ContainerEngine) *Node {

	return &Node{
		Name:            name,
		Resource:        &ResourceCapacity{},
		Works:           make(map[string]*controller.Work),
		Status:          make(map[string]*controller.WorkStatus),
		Hearbeat:        time.Now(),
		store:           store,
		hooks:           &controller.Hooks{},
		containerEngine: engine,
		containers:      make(map[string]container.Container),
		workStatusCh:    make(chan *workStatusNode),
	}
}

//getSystemInfo gathers system information such as CPU, MEM and DISK
func (n *Node) getSystemInfo() error {

	// Gets the resource information
	// Get memory available
	virtualmem, err := mem.VirtualMemory()
	if err != nil {
		return fmt.Errorf("Unable to gather memory information error=%v", err)
	}
	n.Resource.Total.MEM = virtualmem.Available

	//Get the cpu count
	cpus, err := cpu.Counts(true)
	if err != nil {
		return fmt.Errorf("Unable to gather cpu information error=%v", err)
	}
	n.Resource.Total.CPU = cpus

	// Get Disk information
	disks, err := disk.Usage("/")
	if err != nil {
		return fmt.Errorf("Unable to gather disk information error=%v", err)
	}
	n.Resource.Total.DISK = disks.Free

	// At the start Total will be equal to available
	n.Resource.Available.MEM = n.Resource.Total.MEM
	n.Resource.Available.CPU = n.Resource.Total.CPU
	n.Resource.Available.DISK = n.Resource.Total.DISK

	return nil
}

//InitialSync when node-agent restarts/starts it will try to re-sync its state and start from where it had left.
func (n *Node) InitialSync() error {

	//Load list of workloads that has to be managed by this node = W1

	//Load container container list C1 that was started by this agent in the past

	//Load Status list = S1 that was started by

	//We only care about list W1, make sure C1 and S1 map to it.

	return nil
}

//Initialize the node
func (n *Node) Initialize() error {

	var err error

	//Gets the host name
	n.Hostname, err = os.Hostname()
	if err != nil {
		log.Warningf("Error getting hostname = %v", err)
		return err
	}

	if err := n.getSystemInfo(); err != nil {
		log.Warningf("Error getting system information = %v", err)
		return err
	}

	//Registers itself with store
	nodebytes, err := json.Marshal(n)
	if err != nil {
		log.Warningf("Error Marshalling node info = %v", err)
		return err
	}

	//Register this node with the etcd so that scheduler can see this node.
	n.NodeStoreKey = controller.NodesPath + n.Name
	n.NodeWorksKey = n.NodeStoreKey + "/works"
	n.NodeStatusKey = n.NodeStoreKey + "/status"
	e1 := n.store.Put(n.NodeStoreKey, string(nodebytes), true)
	e2 := n.store.Put(n.NodeWorksKey, "", true)
	e3 := n.store.Put(n.NodeStatusKey, "", true)
	if e1 != nil || e2 != nil || e2 != nil {
		log.Fatalf("Error registering the node errors error1=%v, error2=%v, error3=%v", e1, e2, e3)
	}
	//Creates a watch and obtains a work-queue
	n.q = n.store.Watch(n.NodeWorksKey)

	return nil

}

//Register Registers callback
func (n *Node) Register(CreatedHook, UpdatedHook, DeletedHook controller.HookFunction, patterns []string) {

	n.hooks.Created = CreatedHook
	n.hooks.Updated = UpdatedHook
	n.hooks.Deleted = DeletedHook
	n.hooks.Patterns = patterns

}

//Run starts the controller
func (n *Node) Run() error {

	//Update cache, understand the current status before preparing take new actions.

	go n.UpdateStatus(defaultNodeUpdateFrequency)

	//Process items in the work-queue
	for {
		//Blocking call here
		item, shutdown := n.q.Get()
		if shutdown {
			log.Info("Shutting down Node Controoler")
			return nil
		}
		//Regardless of the outcome we are not going to re-process this item so mark it as done.
		n.q.Done(item)
		watchItem, isValid := item.(*storage.KVWatch)
		if !isValid {
			log.Errorf("Error Invalid item found in the watch queue")
			continue

		}
		log.Infof("WatchedItem = %+v", watchItem.ToString())
		currentKey := string(watchItem.Current.Key)
		//Parse the pattern and
		if !controller.MatchPattern(currentKey, n.hooks.Patterns) {
			log.Errorf("Key %s did not match any acceptable patter for node controller", currentKey)
			continue
		}

		//Call corresponding trigger/hooks (node controller is only interested in Created and Deleted events
		switch watchItem.OpType {
		case storage.KVCreated:
			err := n.hooks.Created(currentKey)
			if err != nil {
				log.Errorf("Error occured handling create event error=%v", err)
			}
		case storage.KVDeleted:
			//If deleted we will have the old item
			oldKey := string(watchItem.Old.Key)
			err := n.hooks.Deleted(oldKey)
			if err != nil {
				log.Errorf("Error occured handling delete event error=%v", err)
			}
		default:
			log.Errorf("OpType =%v not handled by node controller", watchItem.OpType)
		}
	}
	return nil
}

//CreateWork creates a container of the specified requirement.
func (n *Node) CreateWork(key string) error {

	//Load work in the memory
	workName := path.Base(key)
	work, err := controller.LoadWork(workName, n.store)
	if err != nil {
		log.Errorf("Unable to load work err = %v", err)
		return err
	}

	//Update our cache
	n.Works[workName] = work

	//Check if such work is already running if yes return an error

	//Check if we have capacity to run this
	if !n.canRunThisWork(work) {
		errMsg := fmt.Sprintf("No Capacity to run work=%s", workName)
		log.Errorf("No Capacity to run work=%v", work.Name)
		status := &workStatusNode{
			Name:             work.Name,
			UpdateNodeStatus: false}
		status.Status = controller.WorkStatusFailed
		status.Output = errMsg
		n.workStatusCh <- status
		return err
	}

	//Create a container
	c1 := n.containerEngine.New()
	err = c1.Start(work)
	if err != nil {

		errMsg := fmt.Sprintf("No Capacity to run work=%s err=%v", workName, err)
		status := &workStatusNode{
			Name:             work.Name,
			UpdateNodeStatus: false}
		status.Status = controller.WorkStatusFailed
		status.Output = errMsg
		n.workStatusCh <- status
		log.Errorf("Unable to start the container")
		return err
	}

	//Update the container information
	n.containers[workName] = c1

	//The container has started wait for it and update the status
	go func() {
		err := c1.Wait()
		if err != nil {
			log.Errorf("Container wait error =%v", err)
		}

		status := &workStatusNode{
			Name:             work.Name,
			UpdateNodeStatus: true}
		status.Status = c1.RunStatus()
		status.Output = c1.Logs()
		n.workStatusCh <- status

	}()

	//Update the available capacity

	return nil
}

//DeleteWork stops a running container and deletes any referene of the container
func (n *Node) DeleteWork(key string) error {

	workName := path.Base(key)

	//Check if such container is available with us
	c, isvalid := n.containers[workName]
	if !isvalid {
		err := fmt.Errorf("No such work(%v) is running", workName)
		log.Fatal(err)
		return err
	}

	//Stop the coontainer
	err := c.Stop()
	if err != nil {
		fmt.Errorf("Unable to stop the container err= %v", err)
		return err
	}

	//Remove the container from internal cache
	delete(n.containers, workName)
	delete(n.Works, workName)

	skey := n.NodeStatusKey + "/" + workName
	err = n.store.Delete(skey, true)
	if err != nil {
		log.Errorf("Unable to delete status of deleted work err=%v", skey)
	}

	return nil
}

//UpdateStatus update the nodes status and output of running processes.
func (n *Node) UpdateStatus(frequency time.Duration) {

	updateFrequencyCh := time.After(frequency)
	//Given frequency Update nodes status such that scheduler can consider resource availablity
	for {
		select {
		case status := <-n.workStatusCh: //Update the status of an individual container.
			if err := n.updateWorkStatus(status); err != nil {
				log.Errorf("Error updating contianer status err=%v", err)
				continue
			}
			if status.UpdateNodeStatus {
				//Some resources have been released update node status of this work
				switch status.Status {
				case controller.WorkStatusFailed:
				case controller.WorkStatusCompleted:
					n.incNodeCapacity(status.Name)
				case controller.WorkStatusRunning:
					n.decNodeCapacity(status.Name)

				}
			}

		case <-updateFrequencyCh: //Update the output of all running containers
			updateFrequencyCh = time.After(frequency)
			for k, c := range n.containers {
				status := c.RunStatus()
				if status == controller.WorkStatusRunning {
					ws := &workStatusNode{Name: k}
					ws.Status = status
					ws.Output = c.Logs()
					if err := n.updateWorkStatus(ws); err != nil {
						log.Errorf("Error updating contianer status err=%v", err)
						continue
					}
				}
			}
		}

	}
}

func (n *Node) canRunThisWork(work *controller.Work) bool {

	n.Resource.Lock()
	defer n.Resource.Unlock()

	if n.Resource.Available.CPU > work.Requirement.CPU &&
		n.Resource.Available.MEM > work.Requirement.MEM &&
		n.Resource.Available.DISK > work.Requirement.DISK {
		return true
	}
	return false
}

func (n *Node) updateWorkStatus(s *workStatusNode) error {

	workStatus := controller.WorkStatus{Status: s.Status, Output: s.Output}
	workStatusBytes, err := json.Marshal(workStatus)
	if err != nil {
		log.Errorf("Marshall error err=%v", err)
		return err
	}
	skey := n.NodeStatusKey + "/" + s.Name
	err = n.store.Put(skey, string(workStatusBytes), false)
	if err != nil {
		log.Errorf("Error writing status for key=%v err=%v", skey, err)
		return err
	}

	return nil
}

func (n *Node) incNodeCapacity(workname string) error {

	work, isvalid := n.Works[workname]
	if !isvalid {
		return fmt.Errorf("invalid workname")
	}

	n.Resource.Lock()
	defer n.Resource.Unlock()

	n.Resource.Available.CPU += work.Requirement.CPU
	n.Resource.Available.MEM += work.Requirement.MEM
	n.Resource.Available.DISK += work.Requirement.DISK
	n.Hearbeat = time.Now()

	//Registers itself with store
	nodebytes, err := json.Marshal(n)
	if err != nil {
		log.Errorf("Error Marshalling node info = %v", err)
		return err
	}
	err = n.store.Put(n.NodeStoreKey, string(nodebytes), true)
	if err != nil {
		log.Errorf("Error writing to store err=%v", err)
		return err
	}

	return nil
}

func (n *Node) decNodeCapacity(workname string) error {

	work, isvalid := n.Works[workname]
	if !isvalid {
		return fmt.Errorf("invalid workname")
	}
	n.Resource.Lock()
	defer n.Resource.Unlock()

	n.Resource.Available.CPU -= work.Requirement.CPU
	n.Resource.Available.MEM -= work.Requirement.MEM
	n.Resource.Available.DISK -= work.Requirement.DISK
	n.Hearbeat = time.Now()

	//Registers itself with store
	nodebytes, err := json.Marshal(n)
	if err != nil {
		log.Errorf("Error Marshalling node info = %v", err)
		return err
	}
	err = n.store.Put(n.NodeStoreKey, string(nodebytes), true)
	if err != nil {
		log.Errorf("Error writing to store err=%v", err)
		return err
	}

	return nil
}
