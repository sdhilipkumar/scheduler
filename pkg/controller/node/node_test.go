package node

import (
	"testing"
	"time"

	"bitbucket.org/sdhilipkumar/scheduler/pkg/container/docker"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/storage"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/storage/etcd"
	"strings"
)

func checkKeyIsValid(t *testing.T, n *Node, key string) []string {
	value, err := n.store.Get(key)
	if err != nil {
		t.Fatalf("It should have been a valid key = %v", err)
	}
	t.Logf("Key = %v value=%v", key, value)
	return value
}

func TestNode(t *testing.T) {

	cluster, etcd := etcd.SetupTestEtcd(t)
	defer cluster.Terminate(t)

	n1 := New("node-1", etcd, &docker.Engine{NodeName: "node-1"})

	t.Run("Initialize", func(t *testing.T) {
		err := n1.Initialize()
		if err != nil {
			t.Fatalf("Expected no error but obtained err=%v", err)
		}

		checkKeyIsValid(t, n1, n1.NodeStoreKey)
		checkKeyIsValid(t, n1, n1.NodeWorksKey)
		checkKeyIsValid(t, n1, n1.NodeStatusKey)
	})

	//A common watch on the node's output location.
	nodeStatusQ := n1.store.Watch(n1.NodeStatusKey)
	itemsChan := make(chan *storage.KVWatch)
	go func() {
		for {
			item, _ := nodeStatusQ.Get()
			wItem, _ := item.(*storage.KVWatch)
			itemsChan <- wItem
		}
	}()

	t.Run("Register", func(t *testing.T) {
		functionTriggered := make(chan string)
		createdFn := func(key string) error {
			t.Logf("CreatedFn called(%s)", key)
			functionTriggered <- key
			return nil
		}
		deletedFn := func(key string) error {
			t.Logf("DeletedFn called(%s)", key)
			functionTriggered <- key
			return nil
		}
		//Register the controller
		n1.Register(createdFn, nil, deletedFn, []string{controller.PatternSingleNodeWorks})
		//Start the controller
		go n1.Run()

		//Trigger Create function
		n1.store.Put(n1.NodeWorksKey+"/test", "false", false)
		select {
		case key := <-functionTriggered:
			t.Logf("Create Hook triggered with key=%v", key)
		case <-time.After(time.Second):
			t.Fatalf("Create function was not triggered")
		}

		n1.store.Delete(n1.NodeWorksKey+"/test", false)
		select {
		case key := <-functionTriggered:
			t.Logf("Delete Hook triggered with key=%v", key)
		case <-time.After(time.Second):
			t.Fatalf("Delete function was not triggered")
		}

	})

	t.Run("CreateWork", func(t *testing.T) {

		n1.Register(n1.CreateWork, nil, n1.DeleteWork, []string{controller.PatternSingleNodeWorks})
		work := &controller.Work{
			Name:        "work1",
			Image:       "bash:3.0",
			Command:     []string{"/usr/local/bin/bash", "-c", "echo Running; sleep 7; echo Completed"},
			Requirement: controller.Resource{CPU: 1},
		}
		err := controller.StoreWork(work, n1.store)
		if err != nil {
			t.Fatalf("Error creating work = %v", err)
		}

		//Make the controller trigger for creating work
		n1.store.Put(n1.NodeWorksKey+"/"+work.Name, "false", false)

		//First message from the iteam should be RUNNING
		select {
		case wItem := <-itemsChan:
			if wItem.OpType != storage.KVCreated {
				t.Fatalf("Item should have been created")
			}
			t.Logf("watch item =%s", wItem.ToString())
		case <-time.After(time.Minute):
			t.Fatalf("Waited enough status has not been updated")

		}

		//The node capacity should be have been reduced accordingly.

		//Second message from the item should be completed
		select {
		case wItem := <-itemsChan:
			if wItem.OpType != storage.KVUpdated {
				t.Fatalf("Item should have been updated")
			}
			t.Logf("watch item =%s", wItem.ToString())
		case <-time.After(time.Minute):
			t.Fatalf("Waited enough status has not been updated")
		}

		//The node capacity should be incremented accordinly

	})

	t.Run("DeleteWork", func(t *testing.T) {
		n1.store.Delete(n1.NodeWorksKey+"/work1", false)

		//Message from the itemsChan should be deleted.
		select {
		case wItem := <-itemsChan:
			if wItem.OpType != storage.KVDeleted {
				t.Fatalf("Item should have been deleted")
			}
			t.Logf("watch item =%s", wItem.ToString())
		case <-time.After(time.Minute):
			t.Fatalf("Waited enough status has not been updated")
		}

	})

	t.Run("UpdateOutput", func(t *testing.T) {
		work := &controller.Work{
			Name:  "work1",
			Image: "bash:3.0",
			Command: []string{"/usr/local/bin/bash", "-c",
				"for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15; do echo Number ${i}; sleep 1; done"},
			Requirement: controller.Resource{CPU: 1},
		}

		err := controller.StoreWork(work, n1.store)
		if err != nil {
			t.Fatalf("Error creating work = %v", err)
		}

		//Make the controller trigger for creating work
		n1.store.Put(n1.NodeWorksKey+"/"+work.Name, "false", false)

		//Make sure you stop the container
		defer func() {
			c, _ := n1.containers[work.Name]
			c.Stop()
		}()

		OldOutputLen := 0
		retry := true
		for retry == true {
			select {
			case wItem := <-itemsChan:
				t.Logf("wItem = %s", wItem.ToString())
				switch wItem.OpType {
				case storage.KVUpdated:
					outputLen := len(strings.Split(string(wItem.Current.Value), "Number"))
					if OldOutputLen == 0 {
						OldOutputLen = outputLen
					} else {
						//Now the new output len should be greater than old output
						if outputLen <= OldOutputLen {
							t.Fatalf("Output is not updating oldOutputlen = %v NewOutputLen=%v",
								OldOutputLen, outputLen)
						}
						retry = false
					}
					t.Logf("OldOutpulen=%d currenoutpulen=%v", OldOutputLen, outputLen)
				default:
					//NoOp
				}
			case <-time.After(time.Minute):
				t.Fatalf("Waited enough status has not been updated")
			}
		}
	})
}
