package controller

import (
	"regexp"

	log "github.com/golang/glog"
)

//Every controller has Input, Output and Action.
//Input  :- the key(s) at which this controller will watch for updates from the store.
//Output :- the location (keys) to which the controller will write to.
//Action :- the action the controller will perform usually invoke the registered callbacks (hooks)

//Some constants Paths to be used (primarily used to watch)
const (
	SchedulerBasePath = "scheduler/"
	WorkloadPath      = SchedulerBasePath + "works/"
	NodesPath         = SchedulerBasePath + "nodes/"
)

//Some constants patterns to be used by controllers,
const (
	AllowedWordRegEx = `([a-zA-Z0-9-]+)/`
	//Will be scheduler/works/<workload-name>/output/
	PatternWorkloadOutPut = WorkloadPath + AllowedWordRegEx + "output/"
	//Will be scheduler/works/<node-name>/
	PatternSingleNodePath = NodesPath + AllowedWordRegEx
	//Will be scheduler/works/<node-name>/works/<workload-name>
	PatternSingleNodeWorks = PatternSingleNodePath + "works/"
	//Will be scheduler/works/<node-name>/status/<workload-name>
	PatternSingleNodeStatus = PatternSingleNodePath + "status/" + AllowedWordRegEx
)

//HookFunction is the type used to register callback, always supplied with key recived from watch.
type HookFunction func(workName string) error

//Hooks represent hook data structure for every single entry.
type Hooks struct {
	Created  HookFunction //Called when a KV is created (only has new KV pair)
	Deleted  HookFunction //Called when a KV is deleted (only has old KV pair)
	Updated  HookFunction //Called with a KV is updated (has both old and new kv pair)
	Patterns []string     //Acceptable patters to match the supplied keys.
}

//Resource represents of Hardware resource
type Resource struct {
	CPU  int    //Number of CPU resource
	MEM  uint64 //Memory in bytes
	DISK uint64 //Disk in bytes
}

//A Controller should implement the following interface
type Controller interface {

	// Register callbacks
	Register(CreatedHook, UpdatedHook, DeletedHook HookFunction, patterns []string)

	// Run Runs the controller
	Run() error
}

//MatchPattern A controller could be interested in receiving updates from more than on key (prefix)
func MatchPattern(key string, pattern []string) bool {

	//Returns true if atleast one pattern matches
	for _, p := range pattern {
		matched, err := regexp.MatchString(p, key)
		if err != nil {
			//Simply log an error MatchString errors out
			log.Warningf("Regular expression Match key=%s error = %v", key, err)
		}
		if matched {
			return true
		}
	}
	return false
}
