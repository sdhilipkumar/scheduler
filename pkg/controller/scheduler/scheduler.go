package scheduler

import (
	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller"
	"bitbucket.org/sdhilipkumar/scheduler/pkg/controller/node"
)

type Scheduler struct {
	Works map[string]*controller.Work
	Nodes map[string]*node.Node
}
