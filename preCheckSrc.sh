#!/bin/bash

echo "Go Fmt....."
for file in `find . -name "*.go"`
do
  gofmt -w ${file}
done

echo "Go Lint....."
for file in `find . -name "*.go"`
do
  golint ${file}
done

echo "Run integration test...."
./integrationTest.sh
