#!/bin/bash

# This is a bottom up integration test. Starts with smaller individual units 
# The order needs to be maintained.
PKGS=(./pkg/storage/etcd  ./pkg/container/docker ./pkg/controller/node)

echo "Integration Test Suite"

for pkg in ${PKGS[*]}
do
  echo Testing .... ${pkg}
  go test ${pkg}
done
